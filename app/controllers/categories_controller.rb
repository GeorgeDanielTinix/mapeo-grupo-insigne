class CategoryController < ApplicationController

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)
    
    if @category.save
      redirect_to @category
    else
      render "New"
    end
  end


  def update
    @category = Category.find(params[:id])
  end

  def destroy
    @category = Category.find(params[:id])
    @category.destroy

    redirect_to root_path
  end

  private

  def category_params
    params.require(:category).permit(:name)
  end
end
