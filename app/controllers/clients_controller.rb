class ClientsController < ApplicationController
  before_action :set_client, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, :except => [:index, :show]

  def index
    @clients = Client.all
    @hash = Gmaps4rails.build_markers(@clients) do |client, marker|
      marker.lat client.latitude
      marker.lng client.longitude
      marker.infowindow client.description
    end
    

    # Format to export CSV File
    respond_to do |format|
      format.html
      format.csv { send_data @clients.to_csv }
      format.xls { send_data @clients.to_csv(col_sep: "\t")}
    end
  end

  def show
    @client = Client.find(params[:id])
  end

  def new
    @client = Client.new
  end

  def edit
  end

  def create
    @client = Client.new(client_params)

    respond_to do |format|
      if @client.save
        format.html { redirect_to @client, notice: 'Client was successfully created.' }
        format.json { render :show, status: :created, location: @client }
      else
        format.html { render :new }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @client.update_attributes(client_params)
        format.html { redirect_to @client, notice: 'Client was successfully updated.' }
        format.json { render :show, status: :ok, location: @client }
      else
        format.html { render :edit }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  # Import CSV file
  def import
    file = params[:file]

    if file.present?
      if file.content_type.ends_with?('csv')
        begin
          Client.import(params[:file])
        rescue
          redirect_to root_path, alert: "El archivo CSV subido no respeta el formato requerido."
          return
        end
        redirect_to root_path, notice: "Lista de Clientes Importada con Exito!!!"
      else
        redirect_to root_path, alert: "Atencion: Debe subir un archivo con extension CSV!"
      end
    else
      redirect_to root_path, alert: "Debe seleccionar un archivo"
    end
  end



  def destroy
    @client = Client.find(params[:id])
    @client.destroy
    
    respond_to do |format|
      format.html { redirect_to clients_url, notice: 'Client was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def ping_in_map
    ping = params[:id]

    return redirect_to root_path if ping == 'all'

    @clients = if ping == 'none'
                []
               else
                 category = Category.find(ping.to_i) rescue nil
                 [category.try(:clients)].flatten
               end

    if @clients.present?
      @hash = Gmaps4rails.build_markers(@clients) do |client, marker|
        marker.lat client.latitude
        marker.lng client.longitude
        marker.infowindow client.description
      end
    end

    render :index
  end

  def seller_on_board
    sell = params[:id]
    return redirect_to root_path if sell == 'all'
    
    @clients = if sell == 'none'
                 []
               else
                 seller = Seller.find(sell.to_i) rescue nil
                 [seller.try(:clients)].flatten
               end
    if @clients.present?
      @hash = Gmaps4rails.build_markers(@clients) do |client, marker|
        marker.lat client.latitude
        marker.lng client.longitude
        marker.infowindow client.description
      end
    end
    
    render :index
  end

  private
    def set_client
      begin
      @client = Client.find(params[:id])
      rescue
      render :index
      end
    end

    def client_params
      params.require(:client).permit(:latitude, :longitude, :client_number, :name, :address, :CP, :zona, :description, :vendedores_id, :seller_id)
    end
end

 
