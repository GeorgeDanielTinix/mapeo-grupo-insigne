json.extract! client, :id, :latitude, :longitude, :client_number, :name, :address, :CP, :zona, :description, :created_at, :updated_at
json.url client_url(client, format: :json)
