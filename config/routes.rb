Rails.application.routes.draw do

  get 'help/index'

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  devise_for :users
  root :to => 'clients#index'

  resources :clients do
    collection do
      post :import
    end
  end

  get '/ping_in_map/:id', to: 'clients#ping_in_map'
  get '/seller_on_board/:id', to: 'clients#seller_on_board'

  authenticate :user do
     scope "/admin" do
        resources :clients, only: [:new, :create, :edit, :update, :destroy]
     end
  end
 end
