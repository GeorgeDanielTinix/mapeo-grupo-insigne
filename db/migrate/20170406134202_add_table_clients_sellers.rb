class AddTableClientsSellers < ActiveRecord::Migration
  def change
    create_join_table :clients, :sellers do |t|
      t.index [:client_id, :seller_id]
      t.index [:seller_id, :client_id]
    end
  end
end

